#!/usr/bin/env python3
import gi
import sys
import csv
import os
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk
from gi.repository import Gdk
from gi.repository import GLib

class MyWindow(Gtk.ApplicationWindow):
    # construct a window (the parent window)

    def __init__(self, app):
        Gtk.Window.__init__(self, title="Control Center", application=app)
        self.set_position(Gtk.WindowPosition.CENTER)
        self.set_border_width(15)

        grid = Gtk.Grid()
        grid.set_column_spacing(10)
        grid.set_row_spacing(4)
        self.add(grid)
        
#buttons
        button1 = Gtk.Button.new_with_label("Install Nvidia")
        button1.set_size_request(140,40)
        button1.set_tooltip_text("Install the Nvidia driver")
        button1.connect("clicked", self.install_nvidia)
        
        button2 = Gtk.Button(label="Cleaner")
        button2.set_tooltip_text("Simple system cleaner")
        button2.set_size_request(140,40)
        button2.connect("clicked", self.cleaner)
        
        button3 = Gtk.Button(label="Kernel manager")
        button3.set_tooltip_text("Install themes and icons from a folder")
        button3.set_size_request(140,40)
        button3.connect("clicked", self.kernel_Window)
        
        button4 = Gtk.Button(label="Firewall")
        button4.set_tooltip_text("Simple firewall manager")
        button4.set_size_request(140,40)
        button4.connect("clicked", self.firewall_window)
        
        button6 = Gtk.Button(label="Distro updater")
        button6.set_tooltip_text("Upgrade to the newest release")
        button6.set_size_request(140,40)
        button6.connect("clicked", self.distro_updater)
        
        button7 = Gtk.Button(label="Apparmor")
        button7.set_tooltip_text("Simple Apparmor manager")
        button7.set_size_request(140,40)
        button7.connect("clicked", self.apparmor_window)
        
        button8 = Gtk.Button(label="System info")
        button8.set_tooltip_text("Get system info")
        button8.set_size_request(140,40)
        button8.connect("clicked", self.info_window)
        
        button9 = Gtk.Button(label="Keyboard setup")
        button9.set_tooltip_text("Set up the keyboard")
        button9.set_size_request(140,40)
        button9.connect("clicked", self.keyboard_setup)
        
#Grid layout                        
        grid.attach(button1, 0, 0, 1, 1)
        grid.attach(button2, 1, 0, 1, 1)
        grid.attach(button3, 2, 0, 1, 1)
        grid.attach(button4, 0, 1, 1, 1)
        grid.attach(button9, 1, 1, 1, 1)
        grid.attach(button6, 2, 1, 1, 1)
        grid.attach(button7, 0, 2, 1, 1)
        grid.attach(button8, 1, 2, 1, 1)
        
#----------------

#---Function nvidia installer    
    def install_nvidia(self, button):
        os.system("xterm -e sudo ubuntu-drivers autoinstall")
#---Function updater    
    def updater(self, button):
        os.system("xterm -e sudo apt upgrade")
#---Function Keyboard setup    
    def keyboard_setup(self, button):
        os.system("kcmshell5 kcm_keyboard")
#---Function cleaner    
    def cleaner(self, button):
        os.system("xterm -e sudo apt autoremove")
        cleaner_start_dialog = Gtk.MessageDialog(parent=self, modal=True, message_type=Gtk.MessageType.INFO, buttons=Gtk.ButtonsType.OK, text="Done")
        cleaner_start_dialog.format_secondary_text("The system is now clean.")
        cleaner_start_dialog.run()
        print("INFO dialog closed")
        cleaner_start_dialog.destroy()
#---Function distro updater    
    def distro_updater(self, button):
        distro_updater_dialog = Gtk.MessageDialog(parent=self, modal=True, message_type=Gtk.MessageType.INFO, buttons=Gtk.ButtonsType.OK, text="CLOSE ALL RUNING PROGRAMS")
        distro_updater_dialog.format_secondary_text("Make sure you close all running programs to make the updater run as painless as posible.")
        distro_updater_dialog.run()
        print("INFO dialog closed")
        distro_updater_dialog.destroy()
        os.system("xterm -e sudo do-release-upgrade")
#----------------   

# Kernel manager
    def kernel_Window(self, widget):
        
        # create a Gtk.Dialog
        UsrWin = Gtk.ApplicationWindow()
        UsrWin.set_title("Kernel manager")
        UsrWin.set_border_width(15)
        
        # The window defined in the constructor (self) is the parent of the dialog.
        # Furthermore, the dialog is on top of the parent window
        UsrWin.set_transient_for(self)
        
        # set modal true: no interaction with other windows of the application
        UsrWin.set_modal(True)
        
        grid = Gtk.Grid()
        grid.set_column_spacing(10)
        grid.set_row_spacing(4)
        UsrWin.add(grid)
        
        button_kernel_updater = Gtk.Button.new_with_label("Check for update")
        button_kernel_updater.set_size_request(90,40)
        button_kernel_updater.connect("clicked", self.update_kernel)

        button_kernel_install = Gtk.Button.new_with_label("Install new kernel")
        button_kernel_install.set_size_request(90,40)
        button_kernel_install.connect("clicked", self.install_kernel)
        
        grid.attach(button_kernel_updater, 0, 0, 1, 1)
        grid.attach(button_kernel_install, 0, 2, 1, 1)
        # show the dialog
        UsrWin.show_all()
#----------------
#---Function to install themes    
    def install_kernel(self, entry):
        os.system("xterm -e ubuntu-mainline-kernel.sh -i")

#---Function to install icons    
    def update_kernel(self, entry):
        os.system("xterm -hold -e ubuntu-mainline-kernel.sh -c")
                

#-apparmor-window
    def apparmor_window(self, widget):
        
        # create a Gtk.Dialog
        aaWin = Gtk.ApplicationWindow()
        aaWin.set_title("Apparmor")
        aaWin.set_border_width(15)
        
        # The window defined in the constructor (self) is the parent of the dialog.
        # Furthermore, the dialog is on top of the parent window
        aaWin.set_transient_for(self)
        
        # set modal true: no interaction with other windows of the application
        aaWin.set_modal(True)
        
        grid = Gtk.Grid()
        grid.set_column_spacing(10)
        grid.set_row_spacing(4)
        aaWin.add(grid)
        
               
        button_aa_start = Gtk.Button.new_with_label("Start")
        button_aa_start.set_size_request(90,40)
        button_aa_start.connect("clicked", self.aa_start)

        button_aa_stop = Gtk.Button.new_with_label("Stop")
        button_aa_stop.set_size_request(90,40)
        button_aa_stop.connect("clicked", self.aa_stop)
        
        
        label_status = Gtk.Label(halign=Gtk.Align.START)
        out = os.popen('pkexec aa-status').read()
        label_status.set_text(out)

        
        grid.attach(button_aa_start, 0, 0, 1, 1)
        grid.attach(button_aa_stop, 1, 0, 1, 1)
        grid.attach(label_status, 0, 1, 2, 1)
        # show the dialog
        aaWin.show_all()
#----------------  

#---Function stop apparmor    
    def aa_stop(self, button):
        os.system("pkexec systemctl stop apparmor && sudo systemctl disable apparmor")
        aa_stop_dialog = Gtk.MessageDialog(parent=self, modal=True, message_type=Gtk.MessageType.INFO, buttons=Gtk.ButtonsType.OK, text="Reboot to take effect")
        #dialog.format_secondary_text("And this is the secondary text that explains things.")
        aa_stop_dialog.run()
        print("INFO dialog closed")
        aa_stop_dialog.destroy()
#----------------

#---Function start apparmor    
    def aa_start(self, button):
        os.system("pkexec systemctl enable apparmor && sudo systemctl start apparmor")
        aa_start_dialog = Gtk.MessageDialog(parent=self, modal=True, message_type=Gtk.MessageType.INFO, buttons=Gtk.ButtonsType.OK, text="Reboot to take effect")
        #dialog.format_secondary_text("And this is the secondary text that explains things.")
        aa_start_dialog.run()
        print("INFO dialog closed")
        aa_start_dialog.destroy()
#----------------

#-firewall-window
    def firewall_window(self, widget):
        
        # create a Gtk.Dialog
        fwWin = Gtk.ApplicationWindow()
        fwWin.set_title("Firewall")
        fwWin.set_border_width(15)
        
        # The window defined in the constructor (self) is the parent of the dialog.
        # Furthermore, the dialog is on top of the parent window
        fwWin.set_transient_for(self)
        
        # set modal true: no interaction with other windows of the application
        fwWin.set_modal(True)
        
        grid = Gtk.Grid()
        grid.set_column_spacing(10)
        grid.set_row_spacing(4)
        fwWin.add(grid)
        
               
        button_fw_start = Gtk.Button.new_with_label("Start")
        button_fw_start.set_size_request(90,40)
        button_fw_start.connect("clicked", self.fw_start)

        button_fw_stop = Gtk.Button.new_with_label("Stop")
        button_fw_stop.set_size_request(90,40)
        button_fw_stop.connect("clicked", self.fw_stop)
        
        
        label_status = Gtk.Label(halign=Gtk.Align.START)
        out = os.popen('pkexec ufw status verbose').read()
        label_status.set_text(out)

        
        grid.attach(button_fw_start, 0, 0, 1, 1)
        grid.attach(button_fw_stop, 1, 0, 1, 1)
        grid.attach(label_status, 0, 1, 2, 1)
        # show the dialog
        fwWin.show_all()
#---------------- 
 
#---Function stop firewall    
    def fw_stop(self, button):
        os.system("pkexec ufw disable")
        fw_stop_dialog = Gtk.MessageDialog(parent=self, modal=True, message_type=Gtk.MessageType.INFO, buttons=Gtk.ButtonsType.OK, text="Firewall stopped")
        #dialog.format_secondary_text("And this is the secondary text that explains things.")
        fw_stop_dialog.run()
        print("INFO dialog closed")
        fw_stop_dialog.destroy()
#----------------

#---Function start firewall    
    def fw_start(self, button):
        os.system("pkexec ufw enable")
        fw_start_dialog = Gtk.MessageDialog(parent=self, modal=True, message_type=Gtk.MessageType.INFO, buttons=Gtk.ButtonsType.OK, text="Firewall started")
        fw_start_dialog.format_secondary_text("The firewall will auto start on boot.")
        fw_start_dialog.run()
        print("INFO dialog closed")
        fw_start_dialog.destroy()
#----------------

#-info-window
    def info_window(self, widget):        
        os.system("kcmshell5 about-distro")
#----------------
class MyApplication(Gtk.Application):

    def __init__(self):
        Gtk.Application.__init__(self)

    def do_activate(self):
        win = MyWindow(self)
        win.show_all()

    def do_startup(self):
        Gtk.Application.do_startup(self)

app = MyApplication()
exit_status = app.run(sys.argv)
sys.exit(exit_status)
